import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Grid,
  TextField,
  IconButton,
  FormControlLabel,
  Checkbox
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Pagination from "@material-ui/lab/Pagination";
import CircularProgress from "@material-ui/core/CircularProgress";
import SortIcon from "@material-ui/icons/Sort";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import axios from "axios";
import GameCard from "./GameCard";

const useStyles = makeStyles(() => ({
  root: {
    height: "100vh",
    width: "100%",
    position: "fixed",
    overflow: "hidden"
  },
  pgHeading: {
    height: "auto",
    margin: "10px 0",
    color: "green"
  },
  mainContent: {
    background: "#f7f7f7",
    height: "auto",
    padding: "30px"
  },
  pagination: {
    background: "#f7f7f7",
    marginTop: "10px",
    padding: "10px",
    borderRadius: "3px",
    textAlign: "center"
  },
  starIcon: {
    color: "yellow"
  }
}));

interface Game {
  title: string;
  platform: string;
  score: number;
  genre: string;
  editors_choice: string;
}

type GamesState = {
  gamesArray: Array<Game>;
  games: Array<Game>;
  page: number;
  sortOrder: string;
};

const totalPageCount = 4;

const PageHeader = () => (
  <Grid container alignItems="center">
    <Grid item xs={6}>
      <h4>Games Arena</h4>
    </Grid>
    <Grid item xs={6}>
      <CircularProgress
        size={30}
        style={{ float: "right", padding: "0 10px" }}
      />
    </Grid>
  </Grid>
);

const Games = () => {
  const [gamesArray, setGamesArray] = useState([]);
  const [games, setGames] = useState([]);
  const [sortOrder, setSortOrder] = useState("asc");
  const [page, setPage] = useState(1);
  const [isGroupBy, setIsGroupBy] = useState(false);
  const [groupedGames, setGroupedGames] = useState(Object);

  const getGamesData = () => {
    axios
      .get("http://starlord.hackerearth.com/gamesarena")
      .then(response => {
        if (response && response.data.length > 0) {
          const data = response.data;
          setGamesArray(data);
          data.splice(0, 1);
          setGames(data);
        }
      })
      .catch(e => {
        console.log("error found", e);
      });
  };

  useEffect(() => {
    getGamesData();
  }, []);

  const handlePageChange = (value: number) => {
    setPage(value);
  };

  const handleSearch = (event: React.ChangeEvent<{}>, value: string) => {
    const searchData = games.filter(
      (game: Game, i: number) => game.title === value
    );
    if (searchData.length > 0) {
      setGames(searchData);
      setPage(1);
    } else {
      setGames(gamesArray);
    }
  };

  const sortGames = () => {
    let order: string = sortOrder;
    if (sortOrder === "asc") {
      games.sort((a: Game, b: Game) => a.score - b.score);
      order = "desc";
    } else if (sortOrder === "desc") {
      games.sort((a: Game, b: Game) => b.score - a.score);
      order = "asc";
    } else setGames(games);
    setSortOrder(order);
  };

  const handleSort = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    sortGames();
  };

  const handleGroupBy = (event: React.ChangeEvent<{}>) => {
    const value = (event.target as HTMLInputElement).checked;
    if (value) {
      setGroupedGames(groupBy(games, "platform"));
    }
    setIsGroupBy(value);
  };

  const classes = useStyles();
  const start = page == 1 ? 0 : totalPageCount * (page - 1),
    end = page == 1 ? totalPageCount : start + totalPageCount;
  return (
    <Grid container className={classes.root}>
      <Grid
        item
        xs={12}
        style={{ height: "auto", padding: "10px", color: "green" }}
      >
        <PageHeader />
      </Grid>
      <Grid
        item
        xs={12}
        style={{ background: "#f7f7f7", height: "80vh", padding: "30px" }}
      >
        <Grid container spacing={5}>
          <Grid item xs={8}>
            <Autocomplete
              id="search"
              autoComplete={true}
              options={gamesArray}
              onInputChange={handleSearch}
              getOptionLabel={(option: Game) => option.title}
              style={{ width: "100%", margin: "auto" }}
              renderInput={params => (
                <TextField {...params} label="Search" variant="outlined" />
              )}
            />
          </Grid>
          <Grid item xs={2}>
            <FormControlLabel
              control={
                <Checkbox
                  color="primary"
                  checked={isGroupBy}
                  placeholder="Groub By Platform"
                  onChange={handleGroupBy}
                />
              }
              label="Group By"
            />
          </Grid>
          <Grid item xs={2}>
            <IconButton onClick={handleSort}>
              {sortOrder === "asc" ? (
                <ArrowDownwardIcon />
              ) : sortOrder === "desc" ? (
                <ArrowUpwardIcon />
              ) : (
                ""
              )}
              <SortIcon />
            </IconButton>
          </Grid>
          {games.slice(start, end).map((game, i) => (
            <GameCard key={i} game={game} classes={classes} />
          ))}
        </Grid>
      </Grid>
      <Grid
        item
        xs={12}
        style={{
          background: "#f7f7f7",
          marginTop: "10px",
          padding: "10px",
          borderRadius: "2px",
          textAlign: "center",
          bottom: 0,
          width: "100%",
          position: "fixed"
        }}
      >
        <Pagination
          page={page}
          count={Math.floor((gamesArray.length - 1) / totalPageCount)}
          onChange={(e: React.ChangeEvent<HTMLInputElement>, val: number) =>
            handlePageChange(val)
          }
        />
      </Grid>
    </Grid>
  );
};

// Helper funtions
function findUniquePlatforms(gamesArray: Array<Game>) {
  const distinctAges = [
    ...new Set(gamesArray.map((game: Game) => game.platform))
  ];
  return distinctAges;
}

function groupBy(items: Array<Game>, key: any) {
  return items.reduce(function(result: Object, item: Game) {
    let k = item[key];
    if (!result[k]) {
      result[k] = [];
    }
    result[k].push(item);
    return result;
  }, {});
}

export default Games;
