import React from "react";
import {
  Grid,
  Typography,
  Card,
  CardContent,
  Divider,
  CardActionArea
} from "@material-ui/core";
import SportsEsportsIcon from "@material-ui/icons/SportsEsports";
import StarIcon from "@material-ui/icons/Star";

interface Game {
  title: string;
  platform: string;
  score: number;
  genre: string;
  editors_choice: string;
}

interface GameCardProps {
  game: Game;
  classes: {
    root: string;
    starIcon: string;
  };
}
const GameCard = ({ game, classes }: GameCardProps) => (
  <Grid item xs={6}>
    <Card>
      <CardContent>
        <Grid container>
          <Grid
            item
            xs={2}
            style={{
              textAlign: "center",
              height: 50
            }}
          >
            <SportsEsportsIcon fontSize="large" />
          </Grid>
          <Grid item xs={9}>
            <Typography variant="body1">{game.title}</Typography>
            <Typography variant="caption">{game.platform}</Typography>
          </Grid>
          <Grid item xs={1}>
            {game.editors_choice === "Y" && (
              <StarIcon classes={{ root: classes.starIcon }} />
            )}
          </Grid>
        </Grid>
      </CardContent>
      <Divider orientation="horizontal" />
      <CardActionArea>
        <Grid container justify="flex-start" style={{ padding: "10px 30px" }}>
          <Grid item xs={6}>
            <Typography>{`Genre: ${game.genre}`}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography style={{ float: "right" }}>{game.score}</Typography>
          </Grid>
        </Grid>
      </CardActionArea>
    </Card>
  </Grid>
);

export default GameCard;
